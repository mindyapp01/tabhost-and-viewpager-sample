package com.example.tabhostviewpagersample;

import android.support.v4.app.Fragment;

public class customTabFragment extends tabFragment{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFragment() {
		// TODO Auto-generated method stub
		addFragmentTab(R.string.a, R.layout.textview,
				Fragment.instantiate(getActivity(), FragmentA.class.getName()), "a");
		addFragmentTab(R.string.b, R.layout.textview, 
				Fragment.instantiate(getActivity(), FragmentB.class.getName()), "b");
	}

}
